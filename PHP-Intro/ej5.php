<?php

$folder = date('Y-m-d G:i');

if(!mkdir($folder, 0777)) {
    die('Error al crear la carpeta.');
}

if (!copy('el_quijote.txt', date('Y-m-d G:i').'/el_quijote.txt.modificado')) {
    die("Error al copiar el archivo.");
}