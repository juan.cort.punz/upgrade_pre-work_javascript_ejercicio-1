<?php

function reemplazar($txt, $txtMod, $find, $repl){

    if(!copy($txt, $txtMod)){

        die("Error al copiar el archivo");

    }else{

        $nev = fopen($txtMod, 'r+');
        $contentNev = fread($nev, filesize($txtMod));
        fclose($nev);
        
        $cont2 = str_replace($find, $repl, $contentNev);
    
        $nev = fopen($txtMod, 'w');
        fwrite($nev, $cont2);
        fclose($nev);
        
        echo "Archivo modificado correctamente.";
    }   
}

reemplazar('el_quijote.txt', 'el_quijote_mod.txt', 'Sancho', 'Morti');