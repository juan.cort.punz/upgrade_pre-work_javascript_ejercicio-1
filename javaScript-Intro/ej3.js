var prompt = require('prompt'); //Para usar la función prompt con node he instalado el módulo con el comando npm install prompt

prompt.start();



var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
                            'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];


prompt.get(['DNI'], function(err, result){ //Pregunto al usuario el número de DNI y recojo los datos

    var pos = letras[result.DNI % 23]; //Se calcula la letra según el dato recogido

    if (result.DNI < 0 || result.DNI > 999999999) { // Se comprueba que sea un número válido

        console.log("El número de DNI proporcionado no es válido.");
        
    }else{

        prompt.get(['Letra'], function (err, result){ // Pregunto al usuario por la letra y se recogen los datos
            
            var letUp = result.Letra.toUpperCase();

            if (pos != letUp) { // Se comprueba que la letra calculada anteriormente coincida con los datos recogidos
    
                console.log("ERROR!! DNI no válido")

            }else{

                console.log("DNI válido")
            }
        })
    }
})


