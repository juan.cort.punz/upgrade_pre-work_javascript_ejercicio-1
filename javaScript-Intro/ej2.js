function tipoRueda(d){

    if (d < 0) {
        console.log("El diámetro introducido es un número negativo."); //Error al intentar introducir un diámetro negativo.
    }

    else if (d <= 10) {
        console.log("Es una rueda para un juguete pequeño."); //Diámetro menor o igual a 10.
    }

    else if (d > 10 && d < 20) {
        console.log("Es una rueda para un juguete mediano."); //Diámetro superior a 10 y menor que 20.

    } else if(d >= 20){
        console.log("Es una rueda para un juguete grande."); //Diámetro mayor o igual que 20.
    }

    else{
        console.log("Por favor, introduzca el diámetro de la rueda."); // Error al no introducir ningún diámetro.
    }

}

tipoRueda(20);